<?php
/**
 * Created by PhpStorm.
 * User: Illia
 * Date: 14.06.2018
 * Time: 19:36
 */
namespace app;
class humanFileSize {
    public function prettyFileSize($fileSize)
    {
        switch ($fileSize) {
            case $fileSize >= 1073741824:
                $result = round($fileSize / 1073741824, 2) . "GB";
                break;
            case $fileSize >= 1048576:
                $result = round($fileSize / 1048576, 2) . "MB";
                break;
            case $fileSize >= 1024:
                $result = round($fileSize / 1024, 2) . "kB";
                break;
            case $fileSize > 1:
                $result = $fileSize . "B";
                break;
            case $fileSize == 1:
                $result = "1B";
                break;
            default:
                $result = "0B";

        }
        return $result;
    }


}