<?php
/**
 * Created by PhpStorm.
 * User: Illia
 * Date: 15.06.2018
 * Time: 13:07
 */

namespace app;
class download
{
    public function getTableCSV($fileRout)
    {


        if (file_exists($fileRout)) {

            if (ob_get_level()) {
                ob_end_clean();
            }
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");
            header('Content-Description: File Transfer');
            header('Content-Type: Content-type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=table.csv');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');


            readfile($fileRout);
            exit;
        }

    }

    public function getTableXLSX($fileRout) {
        if (file_exists($fileRout)) {

            if (ob_get_level()) {
                ob_end_clean();
            }
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");
            header('Content-Description: File Transfer');
            header('Content-Type: application/vnd.ms-excel; charset=utf-8');
            header('Content-Disposition: attachment; filename=table.xlsx');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');


            readfile($fileRout);
            exit;
        }
    }
}