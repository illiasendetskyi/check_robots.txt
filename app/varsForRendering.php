<?php
/**
 * Created by PhpStorm.
 * User: Illia
 * Date: 17.06.2018
 * Time: 10:16
 */
define('OK', 'Ok');
define('ERROR', 'Ошибка');
define('OK_RECOMMEND', 'Доработки не требуются');


/** @noinspection PhpUndefinedVariableInspection */

$robotExistsPrintBackground = ($robotExists) ? 'green' : 'red';
$robotExistsRGBBackground = ($robotExists) ? '00ff00' : 'ff0000';
$robotExistsPrintStatus = ($robotExists) ? OK : ERROR;
$robotExistsPrintCondition = ($robotExists) ? 'Файл robots.txt присутствует' : 'Файл robots.txt отсутствует';
$robotExistsPrintRecommend = ($robotExists) ? OK_RECOMMEND :
    'Программист: Создать файл robots.txt и разместить его на сайте.';



/** @noinspection PhpUndefinedVariableInspection */

$hostExistsPrintBackground = ($hostExists) ? 'green' : 'red';
$hostExistsRGBBackground = ($hostExists) ? '00ff00' : 'ff0000';
$hostExistsPrintStatus = ($hostExists) ? OK : ERROR;
$hostExistsPrintCondition = ($hostExists) ? 'Директива Host указана' : 'В файле robots.txt не указана директива Host';
$hostExistsPrintRecommend = ($hostExists) ? OK_RECOMMEND :
    'Программист: Для того, чтобы поисковые системы знали, какая версия сайта является основных зеркалом,
    необходимо прописать адрес основного зеркала в директиве Host. В данный момент это не прописано. 
    Необходимо добавить в файл robots.txt директиву Host. Директива Host задётся в файле 1 раз,
     после всех правил.';



/** @noinspection PhpUndefinedVariableInspection */

$numberOfHostsPrintBackground = ($numberOfHostsStatus === TRUE) ? 'green' : 'red';
$numberOfHostsRGBBackground = ($numberOfHostsStatus === TRUE) ? '00ff00' : 'ff0000';
$numberOfHostsPrintStatus = ($numberOfHostsStatus === TRUE) ? OK : ERROR;

switch ($numberOfHostsStatus) {
    case $numberOfHostsStatus === TRUE:
        $numberOfHostsPrintCondition = 'В файле прописана 1 директива Host';
        $numberOfHostsStatusRecommend = OK_RECOMMEND;
        break;
    case $numberOfHostsStatus === FALSE:
        $numberOfHostsPrintCondition = 'В файле прописано несколько директив Host';
        $numberOfHostsStatusRecommend = 'Программист: Директива Host должна быть указана в файле толоко 1 раз.
                    Необходимо удалить все дополнительные директивы Host и оставить только 1, 
                    корректную и соответствующую основному зеркалу сайта';
        break;
    case $numberOfHostsStatus == 'Директива Hosts отсутствует':
        $numberOfHostsPrintCondition = 'Директива Hosts отсутствует';
        $numberOfHostsStatusRecommend = 'Программист: Для того, чтобы поисковые системы знали, какая версия сайта 
                является основных зеркалом, 
                необходимо прописать адрес основного зеркала в директиве Host. В данный момент это не прописано. 
                Необходимо добавить в файл robots.txt директиву Host. Директива Host задётся в файле 1 раз, 
                после всех правил.';
        break;
}





/** @noinspection PhpUndefinedVariableInspection */

$fileSizePrintBackground = ($fileSizeStatus === TRUE) ? 'green' : 'red';
$fileSizeRGBBackground = ($fileSizeStatus === TRUE) ? '00ff00' : 'ff0000';
$fileSizePrintStatus = ($fileSizeStatus === TRUE) ? OK : ERROR;

switch ($fileSizeStatus) {
    case $fileSizeStatus === 0:
        $fileSizePrintCondition = 'Файл robots.txt отсутствует';
        $fileSizePrintRecommend = 'Программист: Создать файл robots.txt и разместить его на сайте.';
        break;
    case $fileSizeStatus === FALSE:
        /** @noinspection PhpUndefinedVariableInspection */
        $fileSizePrintCondition = "Размера файла robots.txt составляет {$fileSize}, что превышает допустимую норму";
        $fileSizePrintRecommend = 'Программист: Максимально допустимый размер файла robots.txt составляем 32 кб. 
                    Необходимо отредактировть файл robots.txt таким образом, чтобы его размер не превышал 32 Кб';
        break;
    case $fileSizeStatus === TRUE:
        /** @noinspection PhpUndefinedVariableInspection */
        $fileSizePrintCondition = "Размера файла robots.txt составляет {$fileSize}, что находится в пределах допустимой нормы";
        $fileSizePrintRecommend = OK_RECOMMEND;
        break;
    case $fileSizeStatus == 'Не могу прочитать файл':
        /** @noinspection PhpUndefinedVariableInspection */
        $fileSizePrintCondition = "Размера файла robots.txt составляет {$fileSize}";
        $fileSizePrintRecommend = 'Сканирование этого сайта запрещено, обратитесь в службу поддержки данного сайта';
        break;
}



/** @noinspection PhpUndefinedVariableInspection */


$siteMapPrintBackground = ($siteMapExists) ? 'green' : 'red';
$siteMapRGBBackground = ($siteMapExists) ? '00ff00' : 'ff0000';
$siteMapPrintStatus = ($siteMapExists) ? OK : ERROR;
$siteMapPrintCondition = ($siteMapExists) ? 'Директива Sitemap указана' :
    'В файле robots.txt не указана директива Sitemap';
$siteMapPrintRecommend = ($siteMapExists) ? OK_RECOMMEND : 'Программист: Добавить в файл robots.txt директиву Sitemap';



/** @noinspection PhpUndefinedVariableInspection */

$codeAnswerPrintBackground = ($codeAnswerStatus) ? 'green' : 'red';
$codeAnswerRGBBackground = ($codeAnswerStatus) ? '00ff00' : 'ff0000';
$codeAnswerPrintStatus = ($codeAnswerStatus) ? OK : ERROR;

/** @noinspection PhpUndefinedVariableInspection */
$codeAnswerPrintCondition = ($codeAnswerStatus) ? 'Файл robots.txt отдаёт код ответа сервера 200' :
    "При обращении к файлу robots.txt сервер возвращает код ответа {$codeAnswer}";

$codeAnswerPrintRecommend = ($codeAnswerStatus) ? OK_RECOMMEND :
    'Программист: Файл robots.txt должны отдавать код ответа 200, иначе файл не будет обрабатываться. 
                Необходимо настроить сайт таким образом, 
                чтобы при обращении к файлу robots.txt сервер возвращает код ответа 200';
