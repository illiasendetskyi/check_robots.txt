<?php
/**
 * Created by PhpStorm.
 * User: Illia
 * Date: 19.06.2018
 * Time: 00:13
 */
 function putToExcel($arr) {
        require_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'varsForRendering.php';
        require_once  $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR  . '/app/PHPExcel/Classes/PHPExcel.php';
        $pExcel = new PHPExcel();
        $pExcel->setActiveSheetIndex(0);
        $aSheet = $pExcel->getActiveSheet();
        $aSheet->setTitle('Info about robot.txt');
        $aSheet->getColumnDimension('A')->setWidth(20);
        $aSheet->getColumnDimension('B')->setWidth(10);
        $aSheet->getColumnDimension('C')->setWidth(20);
        $aSheet->getColumnDimension('D')->setWidth(100);


        $aSheet->mergeCells('A2:A3');
        $aSheet->mergeCells('A4:A5');
        $aSheet->mergeCells('A6:A7');
        $aSheet->mergeCells('A8:A9');
        $aSheet->mergeCells('A10:A11');
        $aSheet->mergeCells('A12:A13');
        $aSheet->mergeCells('B2:B3');
        $aSheet->mergeCells('B4:B5');
        $aSheet->mergeCells('B6:B7');
        $aSheet->mergeCells('B8:B9');
        $aSheet->mergeCells('B10:B11');
        $aSheet->mergeCells('B12:B13');
        $aSheet->getStyle('A1:D13')->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        foreach ($arr as $i => $line) {
            if ($i == 0) {
                $aSheet->getStyle('A1')->getFont()->setBold(true);
                $aSheet->getStyle('B1')->getFont()->setBold(true);
                $aSheet->getStyle('C1')->getFont()->setBold(true);
                $aSheet->getStyle('D1')->getFont()->setBold(true);
            }
            $aSheet->setCellValue('A' . ($i + 1), $line[0]);
            $aSheet->setCellValue('B' . ($i + 1), $line[1]);
            $aSheet->setCellValue('C' . ($i + 1), $line[2]);
            $aSheet->setCellValue('D' . ($i + 1), $line[3]);

            $aSheet->getStyle('B' . ($i + 1))->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => $line[4])
                    )
                )
            );

            $pExcel->getActiveSheet()->getRowDimension($i + 1)->setRowHeight(-1);
            $aSheet->getStyle('A' . ($i + 1))->getAlignment()->setWrapText(true);
            $aSheet->getStyle('D' . ($i + 1))->getAlignment()->setWrapText(true);
            $aSheet->getStyle('B' . ($i + 1))->getAlignment()->setHorizontal(
                PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $aSheet->getStyle('B' . ($i + 1))->getAlignment()->setVertical(
                PHPExcel_Style_Alignment::VERTICAL_CENTER);


            $aSheet->getStyle('C'.($i+1))->getAlignment()->setHorizontal(
                PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $aSheet->getStyle('C'.($i+1))->getAlignment()->setVertical(
                PHPExcel_Style_Alignment::VERTICAL_CENTER);


            $aSheet->getStyle('A'.($i+1))->getAlignment()->setHorizontal(
                PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $aSheet->getStyle('A'.($i+1))->getAlignment()->setVertical(
                PHPExcel_Style_Alignment::VERTICAL_CENTER);
        }


        $objWriter = PHPExcel_IOFactory::createWriter($pExcel, 'Excel2007');
        $objWriter->save('table.xlsx');
    }
