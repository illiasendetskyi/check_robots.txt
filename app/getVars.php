<?php
/**
 * Created by PhpStorm.
 * User: Illia
 * Date: 14.06.2018
 * Time: 19:18
 */
namespace app;
class getVars extends humanFileSize {
    private $ch;
    function __construct($ch)
    {
        $this->ch = $ch;
    }

    public function getStatus() {
        $httpCodeStatus = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        return $httpCodeStatus;
    }

    public function getRobotsExists() {
        return $robotExists = ($this->getStatus() === 404) ? FALSE : TRUE;
    }
    public function getHostExists($curlEx) {
        return $hostExists = (substr_count($curlEx, 'Host:') == 0) ? FALSE : TRUE;
    }
    public function getNumberOfHostsStatus($curlEx) {
        //return $numberOfHosts =  ($this->getHostExists($curlEx)) ? (substr_count($curlEx, 'Host:')) : 0;
        if ($this->getHostExists($curlEx)) {
            $numberOfHosts = substr_count($curlEx, 'Host:');
            switch ($numberOfHosts) {
                case $numberOfHosts > 1:
                    $res = FALSE;
                    break;
                case $numberOfHosts == 1:
                    $res = TRUE;
                    break;
                default:
                    $res = 'Директива Hosts отсутствует';
            }
        } else {
            $res = 'Директива Hosts отсутствует';
        }
        return $res;
    }
    public function getFileSizeStatus() {
        if($this->getStatus() !== 404){
            $fileSize= curl_getinfo($this->ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
            switch ($fileSize) {
                case $fileSize === 0:
                    $res = 0;
                    break;
                case $fileSize >= 32768:
                    $res = FALSE;
                    break;
                case $fileSize > 0 && $fileSize <= 32768:
                    $res = TRUE;
                    break;
                default:
                    $res = 'Не могу прочитать файл';

            }

        } else {
            $res = FALSE;
        }
        return $res;
    }
    public function getFileSize() {
        //return $fileSize = $this->prettyFileSize(curl_getinfo($this->ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD));
        if ($this->getRobotsExists()) {
            $fileSize = $this->prettyFileSize(curl_getinfo($this->ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD));
        } else {
            $fileSize = 0;
        }
        return $fileSize;
    }
    public function getSiteMapExists($curlEx) {
        return $siteMapExists = (substr_count($curlEx, 'Sitemap:') == 0) ? FALSE : TRUE;
    }
    public function getCodeAnswerStatus() {
        return $codeAnswerStatus = ($this->getStatus() == 200) ? TRUE : FALSE;
    }

}