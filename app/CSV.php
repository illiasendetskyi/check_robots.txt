<?php
/**
 * Created by PhpStorm.
 * User: Illia
 * Date: 18.06.2018
 * Time: 21:48
 */
namespace app;
class CSV {
    public function putCSV($arr) {
        $handler = fopen($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'table.csv', 'w');
        //fputs( $handler, "\xEF\xBB\xBF" );
        fputs($handler, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
        foreach ($arr as $fields) {
            fputcsv($handler, $fields);
        }
        fclose($handler);
    }
}