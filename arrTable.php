<?php
/**
 * Created by PhpStorm.
 * User: Illia
 * Date: 17.06.2018
 * Time: 12:11
 */
require_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'varsForRendering.php';
return [
    ["Название проверки", "Статус", " ", "Текущее состояние", "ffffff"],
    ["Проверка наличия файла robots.txt", "$robotExistsPrintStatus", 'Состояние', $robotExistsPrintCondition,
        $robotExistsRGBBackground],
    [" ", $robotExistsPrintStatus, 'Рекомендации', $robotExistsPrintRecommend, $robotExistsPrintCondition],
    ["Проверка указания директивы Host", $hostExistsPrintStatus, 'Состояние', $hostExistsPrintCondition,
        $hostExistsRGBBackground],
    [" ", $hostExistsPrintStatus, 'Рекомендации', $hostExistsPrintRecommend, $hostExistsRGBBackground],
    ['Проверка количества директив Host, прописанных в файле', $numberOfHostsPrintStatus, 'Состояние',
        $numberOfHostsPrintCondition, $numberOfHostsRGBBackground],
    [" ", $numberOfHostsPrintStatus, 'Рекомендации', $numberOfHostsStatusRecommend, $numberOfHostsRGBBackground],
    ['Проверка размера файла robots.txt', $fileSizePrintStatus, 'Состояние', $fileSizePrintCondition,
        $fileSizeRGBBackground],
    [' ', $fileSizePrintStatus, 'Рекомендации', $fileSizePrintRecommend, $fileSizeRGBBackground],
    ['Проверка указания директивы Sitemap', $siteMapPrintStatus, 'Состояние', $siteMapPrintCondition,
        $siteMapRGBBackground],
    [' ', $siteMapPrintStatus, 'Рекомендации', $siteMapPrintRecommend, $siteMapRGBBackground],
    ['Проверка кода ответа сервера для файла robots.txt', $codeAnswerPrintStatus, 'Состояние', $codeAnswerPrintCondition,
    $codeAnswerRGBBackground],
    [' ', $codeAnswerPrintStatus, 'Рекомендации', $codeAnswerPrintRecommend, $codeAnswerRGBBackground],
];

