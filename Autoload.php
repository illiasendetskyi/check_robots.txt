<?php
/**
 * Created by PhpStorm.
 * User: Illia
 * Date: 15.06.2018
 * Time: 01:49
 */
class Autoload {
    static public function loader($className) {
        $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $className) . ".php";
        if(file_exists($fileName)) {
            include ($fileName);
            if (class_exists($className)) {
                return true;
            }
        }
        return false;
    }
}