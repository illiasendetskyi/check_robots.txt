<?php
/**
 * Created by PhpStorm.
 * User: Illia
 * Date: 14.06.2018
 * Time: 19:04
 */
require_once(__DIR__ . DIRECTORY_SEPARATOR . 'Autoload.php');
spl_autoload_register(['Autoload', 'loader']);

if (isset($_POST['link'])) {
    $isValid = new \app\valid();
    if ($isValid->isURL($_POST['link'])) {

        $url = $_POST['link'] . '/robots.txt';
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $curlEx = curl_exec($ch);


        $getVars = new \app\getVars($ch);
        $robotExists = $getVars->getRobotsExists();
        $hostExists = $getVars->getHostExists($curlEx);
        $numberOfHostsStatus = $getVars->getNumberOfHostsStatus($curlEx);
        $fileSizeStatus = $getVars->getFileSizeStatus();
        $fileSize = $getVars->getFileSize();
        $siteMapExists = $getVars->getSiteMapExists($curlEx);
        $codeAnswerStatus = $getVars->getCodeAnswerStatus();
        $codeAnswer = $getVars->getStatus();

        curl_close($ch);

        $arr = require_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'arrTable.php');

        $CSV = new \app\CSV();
        $CSV->putCSV($arr);


        require_once __DIR__ . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'Excel.php';
        putToExcel($arr);


        require_once(__DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'table.php');

    } else {
        require_once(__DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'form.php');
        require_once(__DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'error.html');
    }
} elseif ($_POST['downloadCSV']) {

    $fileRout = __DIR__ . DIRECTORY_SEPARATOR . 'table.csv';
    $download = new \app\download();
    $download->getTableCSV($fileRout);
} elseif ($_POST['downloadXLSX']) {

    $fileRout = __DIR__ . DIRECTORY_SEPARATOR . 'table.xlsx';
    $download = new \app\download();
    $download->getTableXLSX($fileRout);
} else {
    require_once(__DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'form.php');
}
