<?php
require_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'varsForRendering.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Table</title>
    <link rel="stylesheet" href="../public/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
</head>
<body>
<table class="table table-bordered">


    <tr>
        <th>Название проверки</th>
        <th>Статус</th>
        <th></th>
        <th>Текущее состояние</th>
    </tr>


    <tr>
        <td rowspan="2">Проверка наличия файла robots.txt</td>
        <td rowspan="2" id="<?php echo $robotExistsPrintBackground; ?>">
            <?php echo $robotExistsPrintStatus; ?>
        </td>
        <td>Состояние</td>
        <td>
            <?php echo $robotExistsPrintCondition; ?>
        </td>
    </tr>
    <tr>
        <td>Рекомендации</td>
        <td>
            <?php echo $robotExistsPrintRecommend; ?>
        </td>
    </tr>


    <tr>
        <td rowspan="2">Проверка указания директивы Host</td>
        <td rowspan="2" id="<?php echo $hostExistsPrintBackground; ?>">
            <?php echo $hostExistsPrintStatus; ?>
        </td>
        <td>Состояние</td>
        <td>
            <?php echo $hostExistsPrintCondition; ?>
        </td>
    </tr>
    <tr>
        <td>Рекомендации</td>
        <td>
            <?php echo $hostExistsPrintRecommend; ?>
        </td>
    </tr>


    <tr>
        <td rowspan="2">Проверка количества директив Host, прописанных в файле</td>
        <td rowspan="2" id="<?php echo $numberOfHostsPrintBackground; ?>">
            <?php echo $numberOfHostsPrintStatus; ?>
        </td>
        <td>Состояние</td>
        <td>
            <?php
            echo $numberOfHostsPrintCondition;
            ?>
        </td>
    </tr>
    <tr>
        <td>Рекомендации</td>
        <td>
            <?php echo $numberOfHostsStatusRecommend; ?>
        </td>
    </tr>


    <tr>
        <td rowspan="2">Проверка размера файла robots.txt</td>
        <td rowspan="2" id="<?php echo $fileSizePrintBackground; ?>">
            <?php echo $fileSizePrintStatus; ?>
        </td>
        <td>Состояние</td>
        <td>
            <?php
            echo $fileSizePrintCondition;
            ?>

        </td>
    </tr>
    <tr>
        <td>Рекомендации</td>
        <td>
            <?php
            echo $fileSizePrintRecommend;
            ?>
        </td>
    </tr>


    <tr>
        <td rowspan="2">Проверка указания директивы Sitemap</td>
        <td rowspan="2" id="<?php echo $siteMapPrintBackground; ?>">
            <?php echo $siteMapPrintStatus; ?>
        </td>
        <td>Состояние</td>
        <td>
            <?php echo $siteMapPrintCondition; ?>
        </td>
    </tr>
    <tr>
        <td>Рекомендации</td>
        <td>
            <?php echo $siteMapPrintRecommend; ?>
        </td>
    </tr>


    <tr>
        <td rowspan="2">Проверка кода ответа сервера для файла robots.txt</td>
        <td rowspan="2" id="<?php echo $codeAnswerPrintBackground; ?>">
            <?php echo $codeAnswerPrintStatus; ?>
        </td>
        <td>Состояние</td>
        <td>
            <?php echo $codeAnswerPrintCondition; ?>
        </td>
    </tr>
    <tr>
        <td>Рекомендации</td>
        <td>
            <?php echo $codeAnswerPrintRecommend; ?>
        </td>
    </tr>


</table>
<div class="btn-group" role="group">
    <form method="post">
        <input type="hidden" id="downloadXLSX" name="downloadXLSX" value="downloadXLSX">
        <input type="submit" value="Сачать таблицу в формате .xlsx" class="btn btn-primary">
    </form>

    <form method="post">
        <input type="hidden" id="downloadCSV" name="downloadCSV" value="downloadCSV">
        <input type="submit" value="Сачать таблицу в формате .csv" class="btn btn-secondary">
    </form>
</div>

</body>
</html>

