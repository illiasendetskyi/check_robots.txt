<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>form</title>
    <link rel="stylesheet" href="../public/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <nav class="navbar fixed-top navbar-dark bg-primary">
        <div class="container">
            <a class="navbar-brand" href="#">Получаем информацию о SEO данного сайта</a>
        </div>
    </nav>

    <div class="row" style="margin-top: 100px">

    </div>

    <div class="row">
        <div class="col-sm">
        </div>
        <div class="col-sm">
            <div class="card text-center">
                <div class="card-body">
                    <h5 class="card-title">Введите URL для получения информации о 'robots.txt'</h5>

                    <form method="POST">
                        <div class="form-group">
                            <input type="text" class="form-control" id="link" name="link" placeholder="Ведите URL" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Получить информацию</button>
                    </form>



                </div>
            </div>
        </div>


        <div class="col-sm">
        </div>
    </div>
</div>

</body>
</html>